SHELL = 			/bin/bash
PKGPATH = 			Assets/typath
JSONFILE = 			$(PKGPATH)/package.json
CHANGELOGFILE = 	$(PKGPATH)/CHANGELOG.md

LAST_TAG_COMMIT = 	$(shell git rev-list --tags --max-count=1)
ifeq ($(LAST_TAG_COMMIT),)
LAST_TAG = 			
VERSION = 			0.0.0
else
LAST_TAG = 			$(shell git describe --tags $(LAST_TAG_COMMIT) )
VERSION = 			$(LAST_TAG)
endif

NEXT_VERSION = 		$(VERSION)
CURRENT_BRANCH = 	$(shell git rev-parse --abbrev-ref HEAD)
MAJOR      = $(shell echo $(VERSION) | sed "s/^\([0-9]*\).*/\1/")
MINOR      = $(shell echo $(VERSION) | sed "s/[0-9]*\.\([0-9]*\).*/\1/")
PATCH      = $(shell echo $(VERSION) | sed "s/[0-9]*\.[0-9]*\.\([0-9]*\).*/\1/")

NEXT_MAJOR_VERSION = $(shell expr $(MAJOR) + 1).0.0
NEXT_MINOR_VERSION = $(MAJOR).$(shell expr $(MINOR) + 1).0
NEXT_PATCH_VERSION = $(MAJOR).$(MINOR).$(shell expr $(PATCH) + 1)

versioninfo:
	@echo "Current version: $(VERSION)"
	@echo "Last tag: $(LAST_TAG)"
	$(if $(LAST_TAG), echo "$(shell git rev-list $(LAST_TAG).. --count) commit(s) since last tag")
	@echo "next major version: |$(NEXT_MAJOR_VERSION)|"
	@echo "next minor version: |$(NEXT_MINOR_VERSION)|"
	@echo "next patch version: |$(NEXT_PATCH_VERSION)|"

deploy:
	@git subtree push --prefix $(PKGPATH) origin upm

major: NEXT_VERSION = $(NEXT_MAJOR_VERSION)
major: versionrev

minor: NEXT_VERSION = $(NEXT_MINOR_VERSION)
minor: versionrev

patch: NEXT_VERSION = $(NEXT_PATCH_VERSION)
patch: versionrev

versionrev: display checkchangelog checkclean revjson tagupm

display:
	@echo "NEXT_VERSION: |$(NEXT_VERSION)|"

revjson:
	@sed -i '' 's/"version": "[0-9\.]*"/"version": "$(NEXT_VERSION)"/' $(JSONFILE)
	@git add $(JSONFILE)
	@git commit -m "update package.json for revision $(NEXT_VERSION)"
	@git push
	@git subtree push --prefix $(PKGPATH) origin upm

checkclean:
	@echo "=> checking for master branch"
	@if [ $(CURRENT_BRANCH) != master ]; then \
		echo "not on master"; \
		false; \
	fi
	@echo "=> checking for no unmerged changes"
	@if git diff-index --quiet HEAD --; then \
		echo "no changes"; \
	else \
		echo "unmerged changes"; \
		false; \
	fi

checkchangelog:
	@echo "=> checking for $(NEXT_VERSION) in $(CHANGELOGFILE)..."
	@grep "## \[$(NEXT_VERSION)\]" $(CHANGELOGFILE)

tagupm:
	@git checkout upm
	@git tag $(NEXT_VERSION)
	@git push origin --tags
	@git checkout master
	@git branch -D upm

