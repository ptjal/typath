/* Copyright © 2018 Tylor Allison */
﻿using UnityEngine;
using System.Collections;

namespace TyPath {

    // ========================================================================================================
	public class Tuple<T1> {
        // PROPERTIES -----------------------------------------------------------------------------------------
		public T1 Item1 { get; set; }

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
		public Tuple(T1 item1) {
			Item1 = item1;
		}
	}

    // ========================================================================================================
	public class Tuple<T1, T2> : Tuple<T1> {
        // PROPERTIES -----------------------------------------------------------------------------------------
		public T2 Item2 { get; set; }

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
		public Tuple(T1 item1, T2 item2) : base(item1) {
			Item2 = item2;
		}
	}

    // ========================================================================================================
	public class Tuple<T1, T2, T3> : Tuple<T1, T2> {
        // PROPERTIES -----------------------------------------------------------------------------------------
		public T3 Item3 { get; set; }

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
		public Tuple(T1 item1, T2 item2, T3 item3) : base(item1, item2) {
			Item3 = item3;
		}
	}

    // ========================================================================================================
	public static class Tuple {
        // STATIC METHODS -------------------------------------------------------------------------------------
		public static Tuple<T1> Create<T1>(T1 item1) {
			return new Tuple<T1>(item1);
		}

		public static Tuple<T1, T2> Create<T1, T2>(T1 item1, T2 item2) {
			return new Tuple<T1, T2>(item1, item2);
		}

		public static Tuple<T1, T2, T3> Create<T1, T2, T3>(T1 item1, T2 item2, T3 item3) {
			return new Tuple<T1, T2, T3>(item1, item2, item3);
		}
	}

}
