/* Copyright © 2018 Tylor Allison */
﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TyPath {

    // ========================================================================================================
    public class BinHeap<T> {

        // INSTANCE VARIABLES ---------------------------------------------------------------------------------
        protected List<Tuple<IComparable, T>> tree;

        // PROPERTIES -----------------------------------------------------------------------------------------
        public bool empty {
            get {
                return (tree.Count == 0);
            }
        }

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public BinHeap() {
            tree = new List<Tuple<IComparable,T>>();
        }

        // INSTANCE METHODS -----------------------------------------------------------------------------------
        protected int Parent(int inIndex) {
            return (inIndex-1)/2;
        }

        protected int LeftChild(int inIndex) {
            return (inIndex*2)+1;
        }

        protected int RightChild(int inIndex) {
            return (inIndex*2)+2;
        }

        protected bool CompareKey(IComparable inKey1, IComparable inKey2) {
            return inKey1.CompareTo(inKey2) < 0;
            //return inKey1 < inKey2;
        }

        protected bool Compare(int inIndex1, int inIndex2) {
            if (inIndex1>=tree.Count) {
                return false;
            }
            if (inIndex2>=tree.Count) {
                return true;
            }
            return CompareKey(tree[inIndex1].Item1, tree[inIndex2].Item1);
        }

        protected void BubbleUp(int inIndex) {
            if (inIndex == 0) return;
            var parent = Parent (inIndex);
            if ( Compare (inIndex, parent)) {
                Swap (inIndex, parent);
                BubbleUp(parent);
            }
        }

        protected void BubbleDown(int inIndex) {
            if (inIndex >= tree.Count/2) return;
            var leftIndex = LeftChild (inIndex);
            var rightIndex = RightChild (inIndex);
            if ( Compare (leftIndex,inIndex) || Compare(rightIndex,inIndex) ) {
                if (Compare (rightIndex, leftIndex)) {
                    Swap (inIndex, rightIndex);
                    BubbleDown (rightIndex);
                } else {
                    Swap (inIndex, leftIndex);
                    BubbleDown (leftIndex);
                }
            }
        }

        protected void Swap(int inIndex1, int inIndex2) {
            var value = tree[inIndex1];
            tree[inIndex1] = tree[inIndex2];
            tree[inIndex2] = value;
        }

        virtual public void Add(IComparable inKey, T inItem) {
            var index = tree.Count;
            var key = new Tuple<IComparable, T>(inKey, inItem);
            tree.Add (key);
            BubbleUp(index);
        }

        virtual public void ChangeKey(IComparable inKey, T inItem) {
    		int index;
            for (index=0; index<tree.Count; index++) {
                if(Object.ReferenceEquals(inItem, tree[index].Item2)) {
                    if (CompareKey(inKey, tree[index].Item1)) {
                        tree[index].Item1 = inKey;
                        BubbleUp(index);
                    } else {
                        tree[index].Item1 = inKey;
                        BubbleDown(index);
                    }
                	break;
                }
            }
        }

        virtual public Tuple<IComparable,T> ExtractRootTuple() {
            Tuple<IComparable, T> rv;
            if (empty) {
                return null;
            }
            Swap(0, tree.Count-1);
            rv = tree[tree.Count-1];
            tree.RemoveAt (tree.Count-1);
            BubbleDown(0);
            return rv;
        }

        virtual public T ExtractRoot() {
            if (empty) {
                return default(T);
            }
            Tuple<IComparable, T> rv = ExtractRootTuple();
            return rv.Item2;
        }

        virtual public IComparable Query(T inItem) {
            int index;
            for (index=0;index<tree.Count;index++) {
                if(Object.ReferenceEquals(inItem, tree[index].Item2)) {
                    return tree[index].Item1;
                }
            }
            return 0;
        }
    }
}
