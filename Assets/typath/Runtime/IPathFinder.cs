/* Copyright © 2018 Tylor Allison */

using System.Collections.Generic;

namespace TyPath {

    // ========================================================================================================
    public interface IPathFinder<T> {
        // METHODS --------------------------------------------------------------------------------------------
        List<T> FindPath(T from, T to);
        List<T> FindPathAndCost(T from, T to, out float cost);
    }
}
