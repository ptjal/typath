/* Copyright © 2018 Tylor Allison */
// NOTE: pulled from MSDN website

using System;
using System.Collections.ObjectModel;

namespace TyPath {
    // ========================================================================================================
    public class NodeList<T> : Collection<Node<T>> {

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public NodeList() : base() { }
        public NodeList(int initialSize) {
            // Add the specified number of items
            for (int i = 0; i < initialSize; i++)
                base.Items.Add(default(Node<T>));
        }

        // INSTANCE METHODS -----------------------------------------------------------------------------------
        public Node<T> FindByValue(T value) {
            // search the list for the value
            foreach (Node<T> node in Items)
                if (node.Value.Equals(value))
                    return node;
            // if we reached here, we didn't find a matching node
            return null;
        }
    }

}
