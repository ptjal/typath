/* Copyright © 2018 Tylor Allison */
namespace TyPath {

    // ========================================================================================================
    public interface IWeightedGraphReader<T> {
        // METHODS --------------------------------------------------------------------------------------------
        T[] GetNeighbors(T value);
        float GetNeighborCost(T value, T neighbor);
        bool Contains(T value);
    }

}
