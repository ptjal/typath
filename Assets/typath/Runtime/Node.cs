/* Copyright © 2018 Tylor Allison */
// NOTE: pulled from MSDN website

namespace TyPath {

    // ========================================================================================================
    public class Node<T> {
        // INSTANCE VARIABLES ---------------------------------------------------------------------------------
        private T data;
        private NodeList<T> neighbors = null;

        // PROPERTIES -----------------------------------------------------------------------------------------
        public T Value {
            get { return data; }
            set { data = value; }
        }
        protected NodeList<T> Neighbors {
            get { return neighbors; }
            set { neighbors = value; }
        }

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public Node() {}
        public Node(T data) : this(data, null) {}
        public Node(T data, NodeList<T> neighbors) {
            this.data = data;
            this.neighbors = neighbors;
        }

    }
}
