/* Copyright © 2018 Tylor Allison */
using System;
using System.Collections.Generic;

namespace TyPath {

    // ========================================================================================================
    public class GraphNode<T> : Node<T>{
        // INSTANCE VARIABLES ---------------------------------------------------------------------------------
        private List<float> costs;

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public GraphNode() : base() { }
        public GraphNode(T value) : base(value) { }
        public GraphNode(T value, NodeList<T> neighbors) : base(value, neighbors) { }

        // INSTANCE METHODS -----------------------------------------------------------------------------------
        new public NodeList<T> Neighbors {
            get {
                if (base.Neighbors == null) {
                    base.Neighbors = new NodeList<T>();
                }
                return base.Neighbors;
            }
        }

        public List<float> Costs {
            get {
                if (costs == null) {
                    costs = new List<float>();
                }
                return costs;
            }
        }
    }

    // ========================================================================================================
    public class WeightedGraph<T> : IEnumerable<T>, IWeightedGraphReader<T> {
        // INSTANCE VARIABLES ---------------------------------------------------------------------------------
        private NodeList<T> nodeSet;

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public WeightedGraph() : this(null) {}
        public WeightedGraph(NodeList<T> nodeSet) {
            if (nodeSet == null) {
                this.nodeSet = new NodeList<T>();
            } else {
                this.nodeSet = nodeSet;
            }
        }

        // INSTANCE METHODS -----------------------------------------------------------------------------------
        protected void AddNode(GraphNode<T> node) {
            nodeSet.Add(node);
        }

        public void AddNode(T value) {
            nodeSet.Add(new GraphNode<T>(value));
        }

        public void AddDirectedEdge(T from, T to, float cost) {
            GraphNode<T> fromNode = (GraphNode<T>) nodeSet.FindByValue(from);
            GraphNode<T> toNode = (GraphNode<T>) nodeSet.FindByValue(to);
            if (fromNode != null && toNode != null) {
                AddDirectedEdge(fromNode, toNode, cost);
            }
        }

        protected void AddDirectedEdge(GraphNode<T> from, GraphNode<T> to, float cost) {
            from.Neighbors.Add(to);
            from.Costs.Add(cost);
        }

        public void AddUndirectedEdge(T from, T to, float cost) {
            GraphNode<T> fromNode = (GraphNode<T>) nodeSet.FindByValue(from);
            GraphNode<T> toNode = (GraphNode<T>) nodeSet.FindByValue(to);
            if (fromNode != null && toNode != null) {
                AddUndirectedEdge(fromNode, toNode, cost);
            }
        }

        protected void AddUndirectedEdge(GraphNode<T> from, GraphNode<T> to, float cost) {
            from.Neighbors.Add(to);
            from.Costs.Add(cost);
            to.Neighbors.Add(from);
            to.Costs.Add(cost);
        }

        public bool Contains(T value) {
            return nodeSet.FindByValue(value) != null;
        }

        public bool Remove(T value) {
            // first remove the node from the nodeset
            GraphNode<T> nodeToRemove = (GraphNode<T>) nodeSet.FindByValue(value);
            if (nodeToRemove == null) {
                // node wasn't found
                return false;
            }
            // otherwise, the node was found
            nodeSet.Remove(nodeToRemove);
            // enumerate through each node in the nodeSet, removing edges to this node
            foreach (GraphNode<T> gnode in nodeSet) {
                int index = gnode.Neighbors.IndexOf(nodeToRemove);
                if (index != -1) {
                    // remove the reference to the node and associated cost
                    gnode.Neighbors.RemoveAt(index);
                    gnode.Costs.RemoveAt(index);
                }
            }
            return true;
        }

        public NodeList<T> Nodes {
            get {
                return nodeSet;
            }
        }

        public int Count {
            get { return nodeSet.Count; }
        }

        public IEnumerator<T> GetEnumerator() {
            foreach (var node in nodeSet) {
                yield return node.Value;
            }
        }

        public T[] GetNeighbors(T value) {
            var node = (GraphNode<T>) nodeSet.FindByValue(value);
            if (node == null) {
                return new T[0];
            }
            var neighbors = new T[node.Neighbors.Count];
            for (var i=0; i<node.Neighbors.Count; i++) {
                neighbors[i] = node.Neighbors[i].Value;
            }
            return neighbors;
        }

        public float GetNeighborCost(T value, T neighbor) {
            var node = (GraphNode<T>) nodeSet.FindByValue(value);
            if (node != null) {
                for (var i=0; i<node.Neighbors.Count; i++) {
                    if (node.Neighbors[i].Value.Equals(neighbor)) {
                        return node.Costs[i];
                    }
                }
            }
            return 0;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }

}
