/* Copyright © 2018 Tylor Allison */
using System;
using System.Collections.Generic;

namespace TyPath {

    // ========================================================================================================
    public class AStarFinder<T> : IPathFinder<T> {
        public delegate float HeuristicFcn(T from, T to);

        // INSTANCE VARIABLES ---------------------------------------------------------------------------------
        HeuristicFcn heuristicFcn;
        IWeightedGraphReader<T> graph;

        // CONSTRUCTORS ---------------------------------------------------------------------------------------
        public AStarFinder(IWeightedGraphReader<T> graph, HeuristicFcn heuristic) {
            this.graph = graph;
            this.heuristicFcn = heuristic;
        }

        // INSTANCE METHODS -----------------------------------------------------------------------------------
        public List<T> FindPathAndCost(T from, T to, out float cost) {
            cost = 0;
            if (!graph.Contains(from) || !graph.Contains(to)) return null;
            // initialize priority queue
            var queue = new BinHeap<T>();

            // initialize association lists
            var cameFrom  = new Dictionary<T,T>();
            var costSoFar  = new Dictionary<T,float>();

            // add starting point to priority queue
            queue.Add(0, from);
            cameFrom[from] = default(T);
            costSoFar[from] = 0;

            // iterate through nodes in the priority queue
            while (!queue.empty) {
                // extract next item from queue
                var current = queue.ExtractRoot();

                // if equal to destination, we are done
                if (current.Equals(to)) {
                    break;
                }

                // otherwise, iterate through neighbors of current node
                var neighbors = graph.GetNeighbors(current);
                foreach (var neighbor in neighbors) {
                    // calculate cost to neighbor
                    var newCost = costSoFar[current] + graph.GetNeighborCost(current, neighbor);
                    if (!costSoFar.ContainsKey(neighbor) || newCost < costSoFar[neighbor]) {
                        costSoFar[neighbor] = newCost;
                        // calculate priority based on guessed heuristic
                        var priority = newCost + heuristicFcn(to, neighbor);
                        queue.Add(priority, neighbor);
                        cameFrom[neighbor] = current;
                    }
                }
            }

            // check for failed path finding (no route to destination)
            if (!cameFrom.ContainsKey(to)) {
                return null;
            }

            // calculate cost
            cost = costSoFar[to];

            // build resulting path
            var path = new List<T>();
            path.Add(to);
            for (var node=cameFrom[to]; !node.Equals(from); node=cameFrom[node]) {
                path.Insert(0, node);
            }
            path.Insert(0, from);
            return path;
        }

        public List<T> FindPath(T from, T to) {
            float cost = 0;
            return FindPathAndCost(from, to, out cost);
        }

    }

}
