# typath - A Unity project/module providing simple path finding using A*.

## Design

Some basic utility classes provide a priority queue and weighted graph classes.
The **AStarFinder** class provides the A* algorithm for path finding.

## Dependencies
* [tymesh](https://bitbucket.org/ptjal/tymesh)
* [tysimpleshape](https://bitbucket.org/ptjal/tysimpleshape)
* [tytest](https://bitbucket.org/ptjal/tytest)
* [tytesthelpers](https://bitbucket.org/ptjal/tytesthelpers)

## Usage

To do...

