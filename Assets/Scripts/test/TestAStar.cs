/* Copyright © 2018 Tylor Allison */
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using TyMesh;
using TyPath;

namespace TyTest {

    public class PathFinderConfig: TestConfig {
        public int seed = 4;
    }

    public class TestAStar: GenericTestWithConfig<PathFinderConfig> {
        TyRendererSet renderers;
        GameObjectPool goPool;

        public override void Start() {
            renderers = new TyRendererSet();
            goPool = new GameObjectPool("test pool", new GameObjectFactory());
            Setup();
            // build the model
            Build();
            // render the model
            Render();
        }

        void Setup() {
            renderers.Add("grid", new TyShapeRenderer("grid", objectCache, goPool));
        }

        void DrawPath(DrawLineFcn drawLine, Vector3[] path) {
            for (var i=1; i<path.Length; i++) {
                drawLine(path[i-1], path[i], Color.blue);
            }
        }

        void Build() {
            var grid = new ExampleGrid(config.seed, 16, 50);
            var drawPoint = TyShapeRenderer.GetDrawPoint((TyShapeRenderer) renderers.Get("grid"));
            var drawLine = TyShapeRenderer.GetDrawLine((TyShapeRenderer) renderers.Get("grid"));
            grid.DrawNodes(drawPoint);
            var graph = grid.MakeGraph();
            var pathfinder = new AStarFinder<Vector3>(graph, (from, to) => (int)(Mathf.Abs(from.x-to.x) + Mathf.Abs(from.z-to.z)));
            var path = pathfinder.FindPath(new Vector3(1,0,1), new Vector3(14,0,14));
            DrawPath(drawLine, path.ToArray());
        }

        void Render() {
            renderers.Render();
        }

        public override void Stop() {
            if (renderers != null) {
                renderers.Flush();
            }
            if (goPool != null) {
                goPool.Clear();
            }
            base.Stop();
        }

    }

}
