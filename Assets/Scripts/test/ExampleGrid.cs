using UnityEngine;

using TyPath;
using TyMesh;

namespace TyTest {
    public class ExampleGrid {
        int[,] nodes;
        int maxCost;
        int dimension;

        // build an example grid w/ random costs
        public ExampleGrid(int seed, int dimension, int maxCost) {
            var originalSeed = UnityEngine.Random.state;
            UnityEngine.Random.InitState(seed);
            nodes = new int[dimension,dimension];
            this.maxCost = maxCost;
            this.dimension = dimension;
            for (var i=0; i<dimension; i++)
            for (var j=0; j<dimension; j++) {
                var cost = UnityEngine.Random.Range(1, maxCost);
                nodes[i,j] = cost;
            }
            UnityEngine.Random.state = originalSeed;
        }

        public void DrawNodes(DrawPointFcn drawPointFcn) {
            for (var i=0; i<dimension; i++)
            for (var j=0; j<dimension; j++) {
                var rgb = (float) nodes[i,j]/(float) maxCost;
                var color = new Color(rgb,0,0);
                drawPointFcn(new Vector3(i,0,j), color);
            }
        }

        public WeightedGraph<Vector3> MakeGraph() {
            var graph = new WeightedGraph<Vector3>();
            for (var i=0; i<dimension; i++)
            for (var j=0; j<dimension; j++) {
                var node = new Vector3(i,0,j);
                graph.AddNode(node);
                if (i>0) {
                    var neighbor = new Vector3(i-1,0,j);
                    var cost = (nodes[i,j] + nodes[i-1,j])/2;
                    graph.AddUndirectedEdge(node, neighbor, cost);
                }
                if (j>0) {
                    var neighbor = new Vector3(i,0,j-1);
                    var cost = (nodes[i,j] + nodes[i,j-1])/2;
                    graph.AddUndirectedEdge(node, neighbor, cost);
                }
            }
            return graph;
        }
    }
}
